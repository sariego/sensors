package com.innovaxxion.beacon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.innovaxxion.beacon.Adapters.MapData;
import com.innovaxxion.beacon.Adapters.SensorAdapterFilter;


public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    @Override
    protected void onStart() {
        super.onStart();
        configUI();
    }

    private void configUI() {

        final ListView validSensors = (ListView) findViewById(R.id.validSensors);
        final EditText sensorUUID   = (EditText) findViewById(R.id.sensorUUID);
        Button addSensor            = (Button)   findViewById(R.id.addSensor);
        Button resetMap             = (Button)   findViewById(R.id.goToMap);

        SensorAdapterFilter.setUpListView(this, validSensors, true);

       ((SensorAdapterFilter) validSensors.getAdapter()).addBeacon("A77B94E5F9744B6DB0DDE3 64FCCAA44-32768-86");
       ((SensorAdapterFilter) validSensors.getAdapter()).addBeacon("A77B94E5F9744B6DB0DDE3 64FCCAA44-32768-74");

        addSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SensorAdapterFilter) validSensors.getAdapter()).addBeacon(sensorUUID.getText().toString());
                Toast.makeText(SettingsActivity.this, "Sensor added", Toast.LENGTH_LONG).show();
            }
        });

        resetMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MapData(SettingsActivity.this).removeAllPins();
                Toast.makeText(SettingsActivity.this, "All pines removed", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    public void sendReport(View v) {
        Intent i = new Intent(this, MapActivity.class);
        startActivity(i);
    }

}
