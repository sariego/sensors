package com.innovaxxion.beacon;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.easibeacon.protocol.IBeacon;
import com.easibeacon.protocol.IBeaconListener;
import com.easibeacon.protocol.IBeaconProtocol;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.innovaxxion.beacon.Adapters.PointModel;
import com.innovaxxion.beacon.Adapters.SensorAdapter;
import com.innovaxxion.beacon.Adapters.SensorAdapterFilter;


import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class SensorActivity extends Activity implements IBeaconListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

    protected static final String TAG = "SensorActivity";
    private IBeaconProtocol ibp = null;
    SensorAdapter sensorAdapter = null;
    public static final int REQUEST_BLUETOOTH_ENABLE = 1;
    private SensorAdapterFilter sensorAdapterFilter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        sensorAdapterFilter = new SensorAdapterFilter(this, false);

        configUI();
        configBeacon();
        configLocation();

    }

    private LocationClient mLocationClient = null;
    Location mCurrentLocation = null;
    private void configLocation() {
        mLocationClient = new LocationClient(this, this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mLocationClient.connect();
    }

    @Override
    protected void onStop() {
        mLocationClient.disconnect();
        super.onStop();
    }

    private void configUI() {
        sensorAdapter = new SensorAdapter(this);
        final ListView sensorList = (ListView) findViewById(R.id.sensors);
        sensorList.setAdapter(sensorAdapter);
        sensorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                        SensorActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                sensorAdapter.notifyDataSetChanged();
                                TextView sensorCount = (TextView) findViewById(R.id.sensor_count);
                                sensorCount.setText("" + sensorAdapter.getCount() + " sensors found.");
                                if (mLocationClient.isConnected()) {
                                    mCurrentLocation = mLocationClient.getLastLocation();
                                }
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        SeekBar seekBar = (SeekBar) findViewById(R.id.setSensibility);
        final TextView percent = (TextView) findViewById(R.id.percent);
        percent.setText("-"+seekBar.getProgress()+"");
        sensorAdapter.sensibility = -1* seekBar.getProgress();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                percent.setText("-"+progress+"");
                sensorAdapter.sensibility = -1 *progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void configBeacon() {
        ibp = IBeaconProtocol.getInstance(this);
        ibp.setListener(this);
        scanBeacons();
    }

    private void scanBeacons(){

        ibp = IBeaconProtocol.getInstance(this);
        if (!IBeaconProtocol.initializeBluetoothAdapter(this)){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_BLUETOOTH_ENABLE );
        } else {
            ibp.setListener(this);
            if(ibp.isScanning())
                ibp.scanIBeacons(false);
            ibp.reset();
            ibp.scanIBeacons(true);
        }
    }

    static final Random rand = new Random();

    /**
     * ADD SOME RANDOMNESS INTO POSITION!
     * @param latlon latitude or longitude
     * @return
     */
    public static double randGeo(Double latlon) {
        int randomNum = rand.nextInt(200) - 100;
        return latlon + ((double)randomNum)*0.001;
    }

    public void sendReport(View v) {

        if (mCurrentLocation == null) {
            Toast.makeText(this, "Location is missing.", Toast.LENGTH_SHORT).show();
            return;
        }

        PointModel p = new PointModel();


        p.setLocation(randGeo(mCurrentLocation.getLatitude()), randGeo(mCurrentLocation.getLongitude()));

        int count = sensorAdapter.getCount();
        for (int i = 0; i < count; i++) {
            IBeacon iBeacon = sensorAdapter.getItem(i);
            if (iBeacon == null) return;
            if (i==0) p.setSensor1_uuid(iBeacon.getRealUUID());//, iBeacon.getMajor(), iBeacon.getMinor());
            if (i==1) p.setSensor2_uuid(iBeacon.getRealUUID());//, iBeacon.getMajor(), iBeacon.getMinor());
        }

        Intent i = new Intent(this, ConfirmActivity.class);
        i.putExtra("point", p.toString());
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sensor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
             Intent i = new Intent(this, SettingsActivity.class);
             startActivity(i);
             return true;
        }
        if (id == R.id.action_map) {
            Intent i = new Intent(this, MapActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void enterRegion(IBeacon ibeacon) {
        Toast.makeText(SensorActivity.this, "Enter " + ibeacon.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void exitRegion(IBeacon ibeacon) {
        Toast.makeText(SensorActivity.this, "Exit " + ibeacon.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void beaconFound(final IBeacon ibeacon) {
        if (sensorAdapterFilter.isBeaconValid(ibeacon.getRealUUID())) {
            sensorAdapter.addBeacon(ibeacon);
        }
    }

    @Override
    public void searchState(int state) {
        String s = null;
        switch (state) {
            case IBeaconProtocol.SEARCH_STARTED     :
                s = "SEARCH_STARTED";
                break;
            case IBeaconProtocol.SEARCH_END_EMPTY   :
                scanBeacons();
                s = "SEARCH_END_EMPTY";
                break;
            case IBeaconProtocol.SEARCH_END_SUCCESS :
                scanBeacons();
                s = "SEARCH_END_SUCCESS";
                break;
        }

        Log.i(TAG, "SEARCH: " + s);

    }

    @Override
    public void operationError(int status) {
        Log.i(TAG, "ERROR " + status);

    }

    @Override
    public void onConnected(Bundle bundle) {
        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDisconnected() {
        Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
    }

    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Toast.makeText(this, "Location ERROR:" + connectionResult.getErrorCode() , Toast.LENGTH_SHORT).show();
        }
    }
}
