package com.innovaxxion.beacon.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.easibeacon.protocol.IBeacon;
import com.innovaxxion.beacon.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pedro on 10/22/14.
 */
public class SensorAdapterConfirm extends BaseAdapter {

    private PointModel beacons = null;
    private Activity context;
    public int sensibility = 0;

    public SensorAdapterConfirm(Activity context) {
        this.context = context;
    }

    public void addBeacon(PointModel iBeacon) {
        beacons = iBeacon;
    }

    public void removeBeacon(IBeacon iBeacon) {
        beacons = null;
    }

    @Override
    public int getCount() {
        return beacons == null ? 0 : 2;
    }

    @Override
    public PointModel getItem(int position) {
        return beacons;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_sensor, parent, false);
        }

        TextView line1 =  (TextView)convertView.findViewById(R.id.line1);
        TextView line2 =  (TextView)convertView.findViewById(R.id.line2);
        convertView.findViewById(R.id.line3).setVisibility(View.INVISIBLE);
        convertView.findViewById(R.id.line4).setVisibility(View.INVISIBLE);
        ((TextView) convertView.findViewById(R.id.number)).setText("" + (position+1));
        convertView.findViewById(R.id.rssi).setVisibility(View.INVISIBLE);

        PointModel beacon = getItem(0);
        if (position == 0) {
            line1.setText("UUID: " + beacon.getParam(PointModel.sensor1_uuid));
            line2.setText(""); // "Minor: " + beacon.getParam(PointModel.sensor1_minor));
        }
        if (position == 1) {
            line1.setText("UUID: " + beacon.getParam(PointModel.sensor2_uuid));
            line2.setText(""); // setText("Minor: " + beacon.getParam(PointModel.sensor2_minor));
        }

        return convertView;
    }
}
