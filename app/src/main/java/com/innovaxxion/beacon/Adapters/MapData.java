package com.innovaxxion.beacon.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pedro on 10/23/14.
 */
public class MapData {

    private Context context;
    private final static String PREFS_NAME =  "innovaxxion.maps";
    private final static String PINS    =  "pins";
    public MapData(Context context) {
        this.context = context;
    }

    public List<PointModel> getPins() {
        List<PointModel> list = new ArrayList<PointModel>();
        try {
            JSONArray points = getPermanentValues();
            for (int i = 0; i < points.length(); i++) {
                list.add(new PointModel(points.getString(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void removeAllPins() {
        try {
            setPermanentValue(new JSONArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addPin(PointModel point) {
        try {
            JSONArray points = getPermanentValues();
            points.put(point.toString());
            setPermanentValue(points);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private JSONArray getPermanentValues() throws Exception {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        String sensors = prefs.getString(PINS, "[]");
        return new JSONArray(sensors);
    }

    private void setPermanentValue(JSONArray json) throws Exception {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString(PINS, json.toString());
        editor.commit();
    }

}
