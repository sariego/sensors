package com.innovaxxion.beacon.Adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.easibeacon.protocol.IBeacon;
import com.innovaxxion.beacon.R;
import com.innovaxxion.beacon.SensorActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pedro on 10/22/14.
 */
public class SensorAdapter extends BaseAdapter {

    private List<IBeacon> beacons = new ArrayList<IBeacon>();
    private Activity context;
    public int sensibility = 0;

    public SensorAdapter(Activity context) {
        this.context = context;
    }

    public void addBeacon(IBeacon iBeacon) {

        boolean isSensibilityOK = (sensibility <= iBeacon.getRssi());

        boolean add = true;
        for (int i = 0 ; i < beacons.size(); i++) {
            IBeacon aBeacon = beacons.get(i)    ;
            if (aBeacon.getMacAddress().equals(iBeacon.getMacAddress())) {
                beacons.set(i, iBeacon);
                add = false;
                break;
            }
        }

        if (add && isSensibilityOK) beacons.add(iBeacon);

    }

    public void removeBeacon(IBeacon iBeacon) {
        beacons.remove(iBeacon);
    }

    @Override
    public int getCount() {
        return beacons.size();
    }

    @Override
    public IBeacon getItem(int position) {
        if (position < beacons.size())
            return beacons.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_sensor, parent, false);
        }

        TextView line1 =  (TextView)convertView.findViewById(R.id.line1);
        TextView line2 =  (TextView)convertView.findViewById(R.id.line2);
        TextView line3 =  (TextView)convertView.findViewById(R.id.line3);
        TextView line4 =  (TextView)convertView.findViewById(R.id.line4);
        TextView number =  (TextView)convertView.findViewById(R.id.number);
        ImageView rssiImage =  (ImageView)convertView.findViewById(R.id.rssi);

        IBeacon beacon = getItem(position);
        // As this method removes elements there can be a fetch with empty values.
        if (beacon == null) {
            line1.setText("");
            line2.setText("");
            line3.setText("");
            line4.setText("");
            return convertView;
        }


        int rssi =  beacon.getRssi();

        line1.setText("UUID: "     + beacon.getRealUUID());
        line2.setText(""); // Minor: "     + beacon.getMinor());
        line3.setText("RSSI:  "     + rssi );
        line4.setText("Proximity: " + beacon.getProximity());

        if (rssi > -65) {
            rssiImage.setBackgroundResource(R.drawable.ic_signal_wifi_4_bar_black_48dp);
        } else if (rssi > -75) {
            rssiImage.setBackgroundResource(R.drawable.ic_signal_wifi_3_bar_black_48dp);
        } else if (rssi > -82) {
            rssiImage.setBackgroundResource(R.drawable.ic_signal_wifi_2_bar_black_48dp);
        } else if (rssi > -90) {
            rssiImage.setBackgroundResource(R.drawable.ic_signal_wifi_1_bar_black_48dp);
        } else { // if (rssi > -88) {
            rssiImage.setBackgroundResource(R.drawable.ic_signal_wifi_0_bar_black_48dp);
        }

        number.setText(""+(position+1));


        if (sensibility > beacon.getRssi()) {
            // REMOVE FARAWAY BEACONS.
            removeBeacon(beacon);
        } else if ((new Date().getTime()-beacon.getDate().getTime()) > 5000) {
            // REMOVE OLD BEACONS.
            removeBeacon(beacon);
        }

        return convertView;
    }
}
