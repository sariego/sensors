package com.innovaxxion.beacon.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.innovaxxion.beacon.R;

import org.json.JSONArray;
import org.w3c.dom.Text;

/**
 * Created by pedro on 10/22/14.
 */
public class SensorAdapterFilter extends BaseAdapter {

    //private List<String> beacons = new ArrayList<String>();
    private Context context;
    private final static String PREFS_NAME =  "innovaxxion.sensors";
    private final static String SENSORS    =  "sensors";
    private boolean canRemove = true;
    public SensorAdapterFilter(Context context, boolean canRemove) {
        this.context = context;
        this.canRemove = canRemove;
    }

    //// PUBLIC METHODS

    static public void setUpListView(Context context, ListView lv, boolean canRemove) {
        final SensorAdapterFilter adapter = new SensorAdapterFilter(context, canRemove);
        lv.setAdapter(adapter);

        if (canRemove) {
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    adapter.removeBeacon(position);
                }
            });
        }
    }


    public boolean isBeaconValid(String checkBeacon) {
        try {
            JSONArray data = getPermanentValues();
            for (int i = 0; i < data.length(); i++) {
                if (checkBeacon.equals(data.getString(i)))
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /////// PRIVATE METHODS


    private JSONArray getPermanentValues() throws Exception {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        String sensors = prefs.getString(SENSORS, "[]");
        return new JSONArray(sensors);
    }

    private void setPermanentValue(JSONArray json) throws Exception {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString(SENSORS, json.toString());
        editor.commit();
    }


    public void addBeacon(String uuid) {
        if (uuid.isEmpty()) return;

        try {
            JSONArray jsensors = getPermanentValues();
            for (int i = 0; i < jsensors.length(); i++) {
                if (jsensors.getString(i).equals(uuid))
                    return;
            }
            jsensors.put(uuid);
            setPermanentValue(jsensors);
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.notifyDataSetChanged();
    }

    private void removeBeacon(String uuid) {
        if (uuid.isEmpty()) return;

        try {
            JSONArray jsensors = getPermanentValues();
            JSONArray jcopy = new JSONArray();

            for (int i = 0; i < jsensors.length(); i++)
                if (!jsensors.getString(i).equals(uuid))
                    jcopy.put(jsensors.getString(i));

            setPermanentValue(jsensors);
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.notifyDataSetChanged();
    }

    private void removeBeacon(int position) {
        try {
            JSONArray jsensors = getPermanentValues();
            JSONArray jcopy = new JSONArray();
            for (int i = 0; i < jsensors.length(); i++)
                if (position != i)
                    jcopy.put(jsensors.getString(i));
            setPermanentValue(jcopy);
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        try {
            JSONArray jsensors = getPermanentValues();
            return jsensors.length();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public String getItem(int position) {
        try {
            JSONArray jsensors = getPermanentValues();
            return jsensors.getString(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_sensor_remove, parent, false);
        }

        TextView line1 =  (TextView)convertView.findViewById(R.id.line1);
        TextView line2 =  (TextView)convertView.findViewById(R.id.line2);
        TextView number = (TextView)convertView.findViewById(R.id.number);

        line1.setText("UUID: "     + getItem(position));
        line2.setText(""); // "Minor: "     + getItem(position));
        number.setText(""+position);

        if (!canRemove) {
            convertView.findViewById(R.id.remove).setVisibility(View.GONE);
        } else {
            Button b = (Button) convertView.findViewById(R.id.remove);
            b.setVisibility(View.VISIBLE);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeBeacon(position);
                }
            });
        }

        return convertView;
    }
}
