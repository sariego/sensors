package com.innovaxxion.beacon.Adapters;

import org.json.JSONObject;

import java.util.Date;
import java.util.Iterator;

/**
 * Created by pedro on 10/23/14.
 */
public class PointModel extends JSONObject {

    public static final String sensor1_uuid  = "SENSOR1_UUID";
    public static final String sensor2_uuid  = "SENSOR2_UUID";
//    public static final String sensor1_minor = "SENSOR1_MINOR";
//    public static final String sensor2_minor = "SENSOR2_MINOR";
//    public static final String sensor1_major = "SENSOR1_MAJOR";
//    public static final String sensor2_major = "SENSOR2_MAJOR";
    public static final String lat           = "LATITUDE";
    public static final String lon           = "LONGITUDE";
    public static final String time          = "TIMESTAMP";

    public String getParam(String s) {
        try {
            return this.getString(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    public void setSensor1_uuid(String sensor1_uuid) { // , Integer major, Integer minor) {
        try {
            this.put(PointModel.sensor1_uuid, sensor1_uuid);
//            this.put(PointModel.sensor1_major, major.toString());
//            this.put(PointModel.sensor1_minor, minor.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSensor2_uuid(String sensor2_uuid) { // , Integer major, Integer minor) {
        try {
            this.put(PointModel.sensor2_uuid, sensor2_uuid);
//            this.put(PointModel.sensor2_major, major.toString());
//            this.put(PointModel.sensor2_minor, minor.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLocation(Double lat, Double lon) {
        try {
            this.put(PointModel.lat, lat.toString());
            this.put(PointModel.lon, lon.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PointModel() {
        try {
            this.put(time, new Date().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PointModel(String restore) {
        try {
            JSONObject jrestore = new JSONObject(restore);
            Iterator<?> keys = jrestore.keys();

            while( keys.hasNext() ){
                String key = (String)keys.next();
                this.put(key, jrestore.get(key));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
