package com.innovaxxion.beacon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.innovaxxion.beacon.Adapters.MapData;
import com.innovaxxion.beacon.Adapters.PointModel;
import com.innovaxxion.beacon.Adapters.SensorAdapterConfirm;
import com.innovaxxion.beacon.Adapters.SensorAdapterFilter;


public class ConfirmActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
    }

    @Override
    protected void onStart() {
        super.onStart();

        PointModel p = new PointModel(getIntent().getStringExtra("point"));
        ((TextView)findViewById(R.id.lat)).setText(p.getParam(PointModel.lat));
        ((TextView)findViewById(R.id.lon)).setText(p.getParam(PointModel.lon));
        ((TextView)findViewById(R.id.time)).setText(p.getParam(PointModel.time));

        ListView lv = (ListView) findViewById(R.id.sensors);
        SensorAdapterConfirm adapter = new SensorAdapterConfirm(this);
        adapter.addBeacon(p);
        lv.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToMap(View view) {
        (new MapData(this)).addPin(new PointModel(getIntent().getStringExtra("point")));
        Intent i = new Intent(this, MapActivity.class);
        startActivity(i);
        finish();
    }
}
