package com.innovaxxion.beacon;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.innovaxxion.beacon.Adapters.MapData;
import com.innovaxxion.beacon.Adapters.PointModel;

import java.util.List;

public class MapActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
    }

    @Override
    protected void onStart() {
        super.onStart();
        drawMap();
    }

    private void drawMap() {
        GoogleMap mMap;

        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdpater(this));
        mMap.setMyLocationEnabled(true);


        List<PointModel> data = (new MapData(this)).getPins();
        if (data.size() == 0) return;

        LatLng moveHere = new LatLng(
                Double.parseDouble( data.get(0).getParam(PointModel.lat)),
                Double.parseDouble( data.get(0).getParam(PointModel.lon))
        );
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(moveHere, 10));


        PolylineOptions rectOptions = new PolylineOptions();

        for (int i = 0; i < data.size(); i++) {
            PointModel p = data.get(i);

            LatLng ll = new LatLng(Double.parseDouble( p.getParam(PointModel.lat) ), Double.parseDouble( p.getParam(PointModel.lon) ));
            rectOptions.add(ll);

            MarkerOptions m = new MarkerOptions().position(ll).title("" + i);
            mMap.addMarker(m);
        }
        rectOptions.color(Color.RED);
        mMap.addPolyline(rectOptions);
    }

    static class CustomInfoWindowAdpater implements GoogleMap.InfoWindowAdapter {
        private final View mymarkerview;
        private Activity context;
        CustomInfoWindowAdpater(Activity context) {
            this.context = context;
            mymarkerview = context.getLayoutInflater().inflate(R.layout.infowindow_pointmodel, null);
        }

        public View getInfoWindow(Marker marker) {
            render(marker, mymarkerview);
            return mymarkerview;
        }

        public View getInfoContents(Marker marker) {
            return null;
        }

        private void render(Marker marker, View view) {
            int i = Integer.parseInt(marker.getTitle());
            List<PointModel> data = (new MapData(context)).getPins();
            PointModel p = data.get(i);

           // ((TextView)view.findViewById(R.id.line1)).setText(p.getParam(PointModel.sensor1_major) +"-"+p.getParam(PointModel.sensor1_minor));
           // ((TextView)view.findViewById(R.id.line2)).setText(p.getParam(PointModel.sensor2_major) +"-"+p.getParam(PointModel.sensor2_minor));

            ((TextView)view.findViewById(R.id.line1)).setText(p.getParam(PointModel.sensor1_uuid));
            ((TextView)view.findViewById(R.id.line2)).setText("");
            ((TextView)view.findViewById(R.id.line3)).setText(p.getParam(PointModel.lat) + "," + p.getParam(PointModel.lon));
            ((TextView)view.findViewById(R.id.line4)).setText(p.getParam(PointModel.time));

            // Add the code to set the required values
            // for each element in your custominfowindow layout file
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
